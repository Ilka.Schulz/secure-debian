#!/bin/bash

# console output colors
NC='\033[0m'      # no color
RED='\033[0;31m'
GRN='\033[0;32m'
BRN='\033[0;33m'
BLU='\033[0;34m'
YLW='\033[1;33m'

# auxiliary functions
function error()  {
    if [ "$#" -lt 1 ]; then
        error "wrong number of arguments in function $FUNCNAME";
    fi
    echo -e "${RED}error: $1${NC}" >&2
    for arg in "${@:2}"; do
        echo "Hint: $arg" >&2
    done
    exit 1
}
function editing_file_msg()  {
    if [ "$#" -ne 1 ]; then
        error "wrong number of arguments in function $FUNCNAME"
    fi
    echo "editing file $1..."
}
function get_clean_config()  {
    if [ "$#" -ne 1 ]; then
        error "wrong number of arguments in function $FUNCNAME"
    fi
    if [[ ! -f "$1" && ! -f "$1.bak" ]]; then
        touch "$1"      # no file and no backup? create empty file!
    elif [[ ! -f "$1.bak" ]]; then
        cp "$1"{,.bak}  # no backup? then create one!
    else
        cp "$1"{.bak,}  # backup available? then restore before editing!
    fi
    editing_file_msg "$1"
}
function run_as_root()  {
    echo "checking privileges..."
    if [[ "$UID" -ne 0 ]] ; then
        error "please run as root"
    fi
}
