#!/bin/bash

set -eu
DIRECTORY=$(dirname $(realpath "$0"))
export DEBIAN_FRONTEND=noninteractive


source "$DIRECTORY/util/common.sh"
run_as_root


echo "reading command line options..."
ARGUMENTS=( "$@" )                               # stash original command line parameters
echo -e "${GRN}invoked with arguments \"${ARGUMENTS[@]}\"${NC}"
# I copied the following code snippet from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash Ilka
MAIL_HOST=""
FORWARD_EMAIL=""
for i in "$@" ; do
    case $i in
        --ignore-lock)
            IGNORE_LOCK=true
        shift
        ;;
        -h=*|--host=*)
            MAIL_HOST="${i#*=}"
        shift # past argument=value
        ;;
        -e=*|--email=*)
            FORWARD_EMAIL="${i#*=}"
        shift
        ;;
        *)
        error "warning: unknown option: $i"      # unknown option
        ;;
    esac
done

if [ -z "$MAIL_HOST" ]; then
    error "Please specify the system's hostname with the --host=<host> option."
fi
if [ -z "$FORWARD_EMAIL" ]; then
    error "Please specify the email address to forward system mails to with the --email=<email> option."
fi



source "$DIRECTORY/forward-mail.d/certbot.sh"
configure_certbot

source "$DIRECTORY/forward-mail.d/postfix.sh"
configure_postfix
