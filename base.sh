#!/bin/bash

set -eu
DIRECTORY=$(dirname $(realpath "$0"))
export DEBIAN_FRONTEND=noninteractive


source "$DIRECTORY/util/common.sh"
run_as_root


source "$DIRECTORY/base.d/apt.sh"
configure_apt

source "$DIRECTORY/base.d/unattended-upgrades.sh"
configure_unattended_upgrades

source "$DIRECTORY/base.d/sshd.sh"
configure_sshd

source "$DIRECTORY/base.d/logcheck.sh"
configure_logcheck
