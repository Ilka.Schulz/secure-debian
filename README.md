# Secure Debian

This project provides minimum security to a new Debian server by setting APT sources, configuring automatic upgrades, securing the OpenSSH server and installing logcheck.

### Scripts

- `base.sh`
  - sets up APT
  - configures automatic upgrades
  - installs and secures OpenSSH server
  - installs logcheck
- `forward-mail.sh`
  - This script requires parameters:
    - `--host=<host>`: the host name of the mail server which must be registered with global DNS servers
    - `--email=<email>`: the external email to which system alerts, etc. shall be forwarded
  - fetches TLS certificate
  - sets up postfix as satellite system (forwarding mail to other mail transfer agents)

### Copyright

This project is developed by Ilka Schulz. You can use it for anything and do with it whatever you want.
