#!/bin/bash

function configure_unattended_upgrades()  {
    echo -e "${BLU}Unattended Upgrades:${NC}"
    
    echo "installing..."
    apt-get -qq install -y unattended-upgrades > /dev/null
    
    echo "configuring..."
    echo "
Unattended-Upgrade::Origins-Pattern {
        \"origin=Debian,codename=\${distro_codename},label=Debian\";
        \"origin=Debian,codename=\${distro_codename},label=Debian-Security\";
};
Unattended-Upgrade::Package-Blacklist {
};
Unattended-Upgrade::Mail root@localhost;
" > "/etc/apt/apt.conf.d/50unattended-upgrades"
    echo "
APT::Periodic::Update-Package-Lists \"1\";
APT::Periodic::Unattended-Upgrade \"1\";
" > "/etc/apt/apt.conf.d/20auto-upgrades"

    echo "restarting service..."
    /etc/init.d/unattended-upgrades restart > /dev/null
}
