#!/bin/bash

function configure_apt()  {
    echo -e "${BLU}APT:${NC}"

    DEBIAN_VERSION=$(lsb_release -c -s)

    echo "finishing broken installs..."
    /usr/bin/dpkg --configure -a
    
    echo "setting sources.list..."
    echo "
# WARNING: This file is managed by Ilka.Schulz/secure-debian. If you want to add
# sources, add them to the /etc/apt/sources.list.d/ directory.

deb http://deb.debian.org/debian ${DEBIAN_VERSION} main
deb-src http://deb.debian.org/debian ${DEBIAN_VERSION} main

deb http://deb.debian.org/debian-security/ ${DEBIAN_VERSION}-security main
deb-src http://deb.debian.org/debian-security/ ${DEBIAN_VERSION}-security main

deb http://deb.debian.org/debian ${DEBIAN_VERSION}-updates main
deb-src http://deb.debian.org/debian ${DEBIAN_VERSION}-updates main
" > /etc/apt/sources.list

    echo "updating and upgrading apt..."
    apt-get -qq clean > /dev/null
    apt-get -qq update > /dev/null
    apt-get -qq -y upgrade > /dev/null
    apt-get -qq -y dist-upgrade > /dev/null
    
    echo "auto-removing old packages..."
    apt-get -qq -y autoremove > /dev/null
}
