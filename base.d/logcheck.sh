#!/bin.bash

function configure_logcheck()  {
    echo -e "${BLU}Logcheck:${NC}"
    
    echo "installing..."
    apt-get -qq install -y logcheck > /dev/null
    
    echo "configuring..."
    echo "
REPORTLEVEL=\"server\"
SENDMAILTO=\"root\"
MAILASATTACH=0
FQDN=1
ADDTAG=\"yes\"
TMP=\"/tmp\"
" > "/etc/logcheck/logcheck.conf"

    # no restart of a service needed as logcheck is just a cronjob
}
