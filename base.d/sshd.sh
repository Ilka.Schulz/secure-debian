#!/bin/bash

# utility
function check_sshd_config()  {
    if [[ $(sshd -t 2>&1) ]]; then
        echo -e "${RED}SSHD config is broken. Please report the following lines to secure-debian@schulz.com.de${NC}"
        sshd -t
        error "invalid SSHD config"
    fi
}
function show_sshd_pubkeys()  {
    if [ "$NEED_TO_CREATE_SSH_KEYS" = true ]; then
        echo -e "${RED}These are the server's public keys' fingerprints. Please write them down!${NC}"
        # TODO: mail to monitor or sth.
    else
        echo -e "These are the server's public keys' fingerprints. They have not changed since $(cat $SSH_CREATEKEYS_ONCE_FLAG_FN)"
    fi
    for fn in /etc/ssh/ssh_host_*_key.pub ; do
        ssh-keygen -l -f "$fn" -E sha256
        ssh-keygen -l -f "$fn" -E md5
    done
}


function get_allowed_users()  {
    FOUND=false

    while read -r LINE ; do
        KEY=$(cut -d'=' -f1 <<< "$LINE")
        
        if [ "$KEY" = "AllowUsers" ]; then
          echo -n "AllowUsers="
          cut -d'=' -f2 <<< "$LINE"
          FOUND=true
        fi
    done < "/etc/ssh/sshd_config"

    if [ "$FOUND" = false ]; then
        echo "# WARNING: You should set the AllowUsers propery in order to restrict login to"
        echo "# these users. You can safely rerun this configuration script and"
        echo "# it will preserve the AllowUser setting."
        echo "#AllowUsers=space separated list of allowed users"
    fi
}

function configure_sshd()  {
    echo -e "${BLU}SSHD:${NC}"

    # install
    echo "installing..."
    apt-get -qq install -y openssh-server > /dev/null

    # create /run/sshd if it does not exist yet (especially in Docker)
    # see https://bugs.launchpad.net/ubuntu/+source/openssh/+bug/45234 for further information
    echo "creating /run/sshd..."
    if [ ! -d "/run/sshd" ]; then
        mkdir -m0755 "/run/sshd"
    fi

    # create new ssh host keypairs once
    SSH_CREATEKEYS_ONCE_FLAG_FN="/etc/ssh/ssh_created_keys.flag"
    NEED_TO_CREATE_SSH_KEYS=false
    if [ ! -f "$SSH_CREATEKEYS_ONCE_FLAG_FN" ]; then
        NEED_TO_CREATE_SSH_KEYS=true
    fi
    if [ "$NEED_TO_CREATE_SSH_KEYS" = true ]; then
        echo "new host keys required, this may take a while..."
        # new RSA moduli
        ssh-keygen -M generate -O bits=4096 /etc/ssh/moduli-4096.candidates
        ssh-keygen -M screen -f /etc/ssh/moduli-4096.candidates /etc/ssh/moduli-4096
        cp /etc/ssh/moduli-4096 /etc/ssh/moduli
        rm /etc/ssh/moduli-4096
        # generate new keypairs
        if  ls /etc/ssh/ssh_host_* > /dev/null 2>&1 ; then
            rm /etc/ssh/ssh_host_*
        fi
        ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -q -N ""
        ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -q -N ""
        # apply mark key creation as done
        date > "$SSH_CREATEKEYS_ONCE_FLAG_FN"
    fi

    show_sshd_pubkeys

    # use basic config and harden
    echo "hardening configuration..."
    CONF_FILE="/etc/ssh/sshd_config"
    get_clean_config "$CONF_FILE"
    echo "
PermitRootLogin no
ClientAliveInterval 90
ClientAliveCountMax 2
PasswordAuthentication no
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key
KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha256
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com
HostKeyAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-512,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com" >> "$CONF_FILE"
    # get allowed users
    echo "reading allowed users..."
    ALLOWED_USERS=$(get_allowed_users)
    if grep -i "#\ WARNING" <<< "$ALLOWED_USERS" > /dev/null ; then
        echo $ALLOWED_USERS
    else
        echo "AllowUsers $ALLOWED_USERS" >> "$CONF_FILE"
    fi

    echo "checking configuration..."
    check_sshd_config
    
    echo "restarting service..."
    /etc/init.d/ssh restart


    # validating security
    echo "auditing SSH server..."
    apt-get -qq install -y ssh-audit > /dev/null
    if ssh-audit localhost | grep "\[fail\]" > /dev/null; then
        ssh-audit localhost 
        echo "Weak SSH configuration. That is bad! Please contact the maintainer of the script (securedebian@schulz.com.de) as soon as possible!"
    fi
    #apt-get -qq remove -y ssh-audit > /dev/null
    
    # install
    echo "installing Fail2Ban..."
    apt-get -qq install -y fail2ban > /dev/null

    # remove local config (will be copied from template in next step)
    CONF_FILE="/etc/fail2ban/jail.local"
    editing_file_msg $CONF_FILE 
    rm -f "$CONF_FILE"

    # add "enabled=true" to [sshd] jail config
    echo "configuring for SSH..."
    while read -r line ; do
        if [ "$line" == "[sshd]" ]; then
            echo $line
            echo "enabled=true"
        else
            echo "$line"
        fi
    done < "/etc/fail2ban/jail.conf" > "$CONF_FILE"

    # apply
    echo "restarting service..."
    /etc/init.d/fail2ban restart

}
