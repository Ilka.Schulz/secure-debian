#!/bin/bash

function configure_certbot()  {
    echo -e "${BLU}Certbot:${NC}"
    
    echo "installing..."
    apt-get -qq install -y certbot > /dev/null
    
    echo "running..."
    certbot certonly --standalone -d "$MAIL_HOST" \
            --non-interactive --agree-tos -m "$FORWARD_EMAIL"
}
