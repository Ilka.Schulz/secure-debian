#!/bin/bash

function configure_postfix()  {
    echo -e "${BLU}Postfix:${NC}"
    
    echo "installing..."
    apt-get -qq install -y postfix > /dev/null
    
    echo "configuring..."
    # main.cf
    echo "setting main.cf..."
    source "$DIRECTORY/forward-mail.d/postfix-main.cf"
    echo "$POSTFIX_MAIN_CF" > /etc/postfix/main.cf
    chown root:root /etc/postfix/main.cf
    chmod 644 /etc/postfix/main.cf
    # master.cf
    echo "setting master.cf..."
    rm -f /etc/postfix/master.cf
    cp "$DIRECTORY/forward-mail.d/postfix-master.cf" /etc/postfix/master.cf
    chown root:root /etc/postfix/master.cf
    chmod 644 /etc/postfix/master.cf
    # /etc/mailname
    echo "$MAIL_HOST" > /etc/mailname
    # /etc/hostname
    echo "$MAIL_HOST" > /etc/hostname
    hostname "$MAIL_HOST" 

    echo "reloading service configuration..."
    systemctl reload postfix.service

    echo "verifying that service is running..."
    sleep 1
    if ! systemctl is-active --quiet postfix.service; then
        error "postfix does not run"
    fi

    echo "adding aliases..."
    CONF_FILE="/etc/aliases"
    get_clean_config "$CONF_FILE"
    echo "
info:           root
contact:        root
spam:           root
admin:          root
administrator:  root
ssladmin:       root
dmarc:          root
root:           $FORWARD_EMAIL
" >> "$CONF_FILE"

    echo "applying aliases..."
    newaliases
}
